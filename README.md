# 環境構築手順

## インストールするもの
・virtualbox  
https://www.virtualbox.org/

・vagrant  
https://www.vagrantup.com/downloads

・git  
https://git-scm.com/downloads

・vscode  
https://code.visualstudio.com/download  


## Vagrantfileをクローン
ターミナルを起動(windowsはgit bashを開く)
ここからコマンドを実行

git clone https://etsuro@bitbucket.org/etsuro/vagrant-laravel.git

## vagrantコマンド実行
cd vagrant-laravel  
vagrant plugin install vagrant-vbguest  
vagrant plugin update

vagrant up  
vagrant ssh 

## ここからCentOS環境 Laravelをインストール
cd /workspace  
composer create-project --prefer-dist laravel/laravel laravel "6.*"  
chmod -R 777 laravel  

下記URLにアクセスしてLaravelがインストールされていれば完了  
http://192.168.33.10/


## vscodeでssh接続する
プラグインで「Remote - SSH」をインストール  
一番左下の「><」のようなマークをクリック  
「Remote-SSH:Open Configuration File」をクリックし一番上のファイルを選択後、```vagrant ssh-config```を実行  
下記のような設定が表示されるためそれをペースト

```
Host vagrant-laravel
  HostName 127.0.0.1
  User vagrant
  Port 2222
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /opt/workspace/vagrant/.vagrant/machines/default/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

「Remote-SSH:Connect to Host」をクリックしvagrant-laravelを選択  
別ウィンドウが立ち上がるサイドバーのフォルダ選択で「/workspace/laravel/」を入力  
vscodeでLaravelを修正する
