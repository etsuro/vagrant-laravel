#!/usr/bin/env bash

dir_provision="/home/vagrant/.vagrant_provision"

if [ ! -d $dir_provision ]; then
    mkdir $dir_provision
fi

# Disabled SELinux
setenforce 0
sed -i.org -e "/^SELINUX=/s/enforcing/disabled/" /etc/selinux/config

# epel repo
yum -y install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

# Apache
if [ ! -e $dir_provision/httpd ]; then
    touch $dir_provision/httpd
    yum -y install httpd
    systemctl enable httpd
fi

httpdconf="
<VirtualHost *:80>
    #ServerName itp.localhost
    DocumentRoot "/workspace/laravel/public"
    <Directory "/workspace/laravel/public">
    Options FollowSymLinks
    AllowOverride all
    Require all granted
    </Directory>
</VirtualHost>
"
if [ ! -e /etc/httpd/conf.d/laravel_httpd.conf ]; then
    echo "$httpdconf" > "/etc/httpd/conf.d/laravel_httpd.conf"
fi

# php7.4
if [ ! -e $dir_provision/php74 ]; then
    touch $dir_provision/php74
    yum -y install --enablerepo=remi,remi-php74 php php-mbstring php-xml php-pdo php-mysql
fi
# composer
if [ ! -e /usr/local/bin/composer ]; then
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
    mv composer.phar /usr/local/bin/composer
fi

# MySQL
if [ ! -e /home/vagrant/.mysql ]; then
    touch /home/vagrant/.mysql
    yum -y install https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
    yum -y install mysql-community-server
    ## initialize root not password.
    mysqld --initialize-insecure --user=mysql
    systemctl enable mysqld
fi

# node 8
curl --silent --location https://rpm.nodesource.com/setup_8.x | sudo bash -
yum -y install nodejs

# Git
yum -y install git
yum -y install zip unzip 

mkdir /workspace
cd /workspace
chmod -R 777 /workspace

# Restart Service
systemctl restart httpd
systemctl restart mysqld
